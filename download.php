<?php

function _get($str){
    $val = !empty($_GET[$str]) ? $_GET[$str] : null;
    return $val;
}


#debug output
function _print($name,$str){ 
	$_debug=_get("debug");
	if($_debug){
		echo $name."</br>\n";
		var_dump($str);
		echo "</br></br>\n\n";
	}else
		;
}

$file_dir =  dirname(__FILE__)."/apk/";//dir of files to be downloaded
$cmd='ls -lrt '.$file_dir.' | grep .*\.apk$ | tail -n 1 | awk \'{print $NF}\'';
$file_name = exec($cmd);//filename of the file to be download

_print("file_dir",$file_dir);
_print("cmd",$cmd);
_print("file_name",$file_name);

//check if the file is exist
if (! file_exists ($file_dir.$file_name."")) {
    echo "file not found!";
    exit ();
} else {  
    //open the file to be downloaded
    $file = fopen ( $file_dir . $file_name, "r" );
    //html header
    Header ( "Content-type: application/octet-stream" );
    Header ( "Accept-Ranges: bytes" );
    Header ( "Accept-Length: " . filesize ( $file_dir . $file_name ) );
    Header ( "Content-Disposition: attachment; filename=" . $file_name );
    //output the file
    echo fread ( $file, filesize ( $file_dir . $file_name ) );
    fclose ( $file );
    exit ();
}
?>
