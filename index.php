<html>
<head>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
<!--For more information about Pure CSS, see https://purecss.io/ -->
<style type="text/css">
.c1 {
	background-color: #1abc9c;
	color: #FFF;
}
body{ 
	text-align:center
} 
.form {
	max-width: 60%;
	margin: 5px auto;
	border-style: solid; 
	border-width: 1px;
}

</style>
<title>溯源查询APP上传</title>
</head>

<body>
<div class="pure-g">
	<div class="pure-u-1-1 c1"><p><h1>溯源查询APP上传</h1></p> </div>
</div>
<div class="pure-g">
	<div class="pure-u-1-1">
		<form class="pure-form pure-form-aligned form" action="upload-app.php" method="post" enctype="multipart/form-data" target="iframe_a">
			<fieldset>
				<input class="pure-input-2-3" type="file" name="idartlab_app" id="idartlab_app" > 
			</fieldset>
			<fieldset>
				<textarea class="pure-input-2-3" placeholder="description of the file" name="desc"></textarea>
			</fieldset>
			<button type="submit" class="pure-button pure-input-1-3 pure-button-primary">上传</button>
		</form>
		<iframe src="" name="iframe_a" width="60%" height="50%"></iframe>
	</div>
</div>

<?php
	$file_dir =  dirname(__FILE__)."/apk/";//dir of files to be downloaded
	$cmd='ls -lrt '.$file_dir.' | grep .*\.apk$ | tail -n 1 | awk \'{sub(/apk$/,"");print $NF}\'';
	$file_name = exec($cmd);//filename of the file to be download
	$txt=$file_name.".apk\n";
	//check if the file is exist
	if (file_exists ($file_dir.$file_name."txt")) {
    	//open the file to be downloaded
	    $file = fopen ( $file_dir . $file_name."txt", "r" );
	    //output the file
	    $txt=$txt.fread ( $file, filesize ( $file_dir . $file_name."txt" ) );
	    fclose ( $file );
	}
?>

<div class="pure-g">
	<div class="pure-u-1-1">
		<form class="pure-form pure-form-aligned form">
			<fieldset>
				<textarea class="pure-input-2-3" placeholder="<?=$txt?>" readonly></textarea>
			</fieldset>
			<fieldset>
				<a class="pure-button pure-button-active pure-input-1-3" href="download.php">下载</a>
			</fieldset>
		</form>
	</div>
</div>
</body>
</html>
